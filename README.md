# API REST - LARAVEL 8
This is an API REST made with laravel 8.0, passport and l5 repository.
The laravel Framework give us an elegant and organized way to write codes else we use the passaport tool to get an secure and smart authentication. For the last is the l5 repository that automate a lot of important tasks as create in a only command a complete CRUD with wondeful possibilities like filter your queries by passing parameter from requests.

## INSTALLING
We can use this api in few moments going through some steps.
1. Clone or download this repository to your project folder.
2. Run **composer install** on root of this project.
3. Setup your **.env** file (check db setup).

That`s ready to use!!

### OTHER DOCS
1. https://documenter.getpostman.com/view/18857919/UVRBoSBV (API Documentation)
2. https://laravel.com/docs/8.x (Laravel Documentation)
