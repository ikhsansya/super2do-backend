<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'date',
        'time',
        'status'
    ];

    public $rules = [
        'title'     => 'required',
        'date'      => 'required',
        'time'      => 'required',
        'status'    => 'required'
    ];
}
